var clients = function () {



    $('#saveClient').on('click', function(){
        let data = $('#client').serialize();
        
        $.ajax({
            method: "POST",
            url: $urlBase+"/client-store",
            data: data,
          }).done(function(res) {
            if(res == 1){
              alert('Cliente registrado con exito');
              $("#client")[0].reset();
            }else{
              alert('Error al crear al cliente');
            }
            
          });
    });

    

    $('#trashClient').on('click', function(){
        $("#client")[0].reset();
    });

    function frutasGet(){

      var cursos = $("#fruta_id");
      $.ajax({
        method:  'GET',
        url:   $urlBase+"/frutas-get",
        success:  function (r) 
          {
              
              cursos.find('option').remove();

              $(r).each(function(i, v){ 
                  cursos.append('<option value="' + v.id + '">' + v.nombre + '</option>');
              })

              cursos.prop('disabled', false);
          },
          error: function()
          {
              alert('Ocurrio un error en el servidor ..');
              alumnos.prop('disabled', false);
          }
      });
    }

    function clienteGet(){

      var cursos = $("#client_id");
      $.ajax({
        method:  'GET',
        url:   $urlBase+"/clients-get",
        success:  function (r) 
          {
              
              cursos.find('option').remove();

              $(r).each(function(i, v){ 
                  cursos.append('<option value="' + v.id + '">' + v.nombre + '</option>');
              })

              cursos.prop('disabled', false);
          },
          error: function()
          {
              alert('Ocurrio un error en el servidor ..');
              alumnos.prop('disabled', false);
          }
      });
    }

    $('#cantidad').on('change', function(){
      let total = parseFloat($('#valor_kilo').val()) * parseFloat($('#cantidad').val());
      $('#subtotal').val(total);
    });
    
    $('#btnBorrarPedido').on('click', function(){
      $("#orden")[0].reset();
    });











return {

  init: function () {
    frutasGet();
    clienteGet();
  }
};

}();

jQuery(document).ready(function() {


  clients.init();


});