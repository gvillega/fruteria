var orden_pago = function () {
   
    var initPago = function(posicion){
        var table = $('#pedido');

        var oTable = table.dataTable({
            destroy: true,
            
            "loadingMessage": 'Cargando...',
            "ajax": {
               url:  $urlBase+'/pedido-get',
               data:{'posicion':posicion},
               "dataSrc": "list"               
            },
            
            "columns": [
                { "data": "id", "title": "Id", "visible": false, "orderable": true },
                { "data": "fruta_id", "title": "Producto", "visible": true, "orderable": true },
                { "data": "valor_kilo", "title": "Valor por Kilo", "visible": true, "orderable": true },
                { "data": "cantidad", "title": "Cantidad", "visible": true, "orderable": true },
                { "data": "subtotal", "title": "SubTotal", "visible": true, "orderable": true },
                { "data": "", "title": "Acciones", "visible": true, "orderable": true ,
                    render: function (data, type, row) {
                        let eliminar = '<input type="button" class="btn btn-sm btn-danger" id="btDelete" value="Eliminar">';
                        let editar   = '<input type="button" class="btn btn-sm btn-primary" id="btEdit" value="Editar">';
                        return eliminar+' '+editar;
                    }
                }
            ],
            
            responsive: false,

           "order": [
                /*[0, 'asc']*/
            ],

            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
        });

        table.on('click', '#btEdit, #btDelete', function() {
            var data = oTable.DataTable().row($(this).parents('tr')).data();

            if(this.id == 'btDelete'){
                $.ajax({
                    method: "GET",
                    url: $urlBase+"/pedido-delete",
                    data: {"id":data.id},
                }).done(function(res) {
                    if(res == 1){
                      let posicion = $('#posicion').val();
                      initPago(posicion);
                      alert("Registro Eliminado");
                    }
                    
                });
              
            }

            if(this.id == 'btEdit'){
                //window.open($urlBase+"/pagos-showPago"+'/'+data.id, '_blank');
                //return false;
                alert(this.id);
            }
           

        });
    }


    $('#generar').on('click', function() {
       
        var id      =   $('#inp-seleccion_cliente').val();
        var desde   =   $('#inp-desde').val();
        var hasta   =   $('#inp-hasta').val();
        
        if(id.length == '0'){
            id = 0;
        }
        if(desde.length == '0'){
            desde = 0;
        }
        if(hasta.length == '0'){
            hasta = 0;
        }


        initPago(posicion);
    });

    $('#zip').on('click', function() {
       
        var id      =   $('#inp-seleccion_cliente').val();
        var desde   =   $('#inp-desde').val();
        var hasta   =   $('#inp-hasta').val();
        
        if(id.length == '0'){
            id = 0;
        }
        if(desde.length == '0'){
            desde = 0;
        }
        if(hasta.length == '0'){
            hasta = 0;
        }


        window.location.replace($urlBase+"/pagos-downloadZip"+'/'+desde+'/'+hasta);
        
        
    });

    $('#export').on('click', function() {
        var id      =   $('#inp-seleccion_cliente').val();
        var desde   =   $('#inp-desde').val();
        var hasta   =   $('#inp-hasta').val();
        
        if(id.length == '0'){
            id = 0;
        }
        if(desde.length == '0'){
            desde = 0;
        }
        if(hasta.length == '0'){
            hasta = 0;
        }
        
        window.location.replace($urlBase+"/pagos-exportar"+'/'+id+'/'+desde+'/'+hasta);
    });

    /* */
    $('#savePedido').on('click', function(){
        let data = $('#orden').serialize();
        
        $.ajax({
            method: "POST",
            url: $urlBase+"/pedido-store",
            data: data,
          }).done(function(res) {
            if(res == 1){
              let posicion = $('#posicion').val();
              initPago(posicion);
              $("#orden")[0].reset();
            }
            
          });
      });

    return {

        init: function () {
            let posicion = $('#posicion').val();
            initPago(posicion);

        }
    };

}();

jQuery(document).ready(function() {
    
    
    orden_pago.init();
    

});