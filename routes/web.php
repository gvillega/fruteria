<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/
Route::get('/', 'OrderController@index');

Route::post('client-store', 'ClientController@storeClient');
Route::get('clients-get', 'ClientController@clientGet');



Route::get('pedido-get', 'OrderController@get');
Route::get('frutas-get', 'OrderController@productoGet');

Route::post('pedido-store', 'OrderController@store');
Route::post('pedido-delete', 'OrderController@delete');

