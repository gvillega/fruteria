<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;

class ClientController extends Controller
{
    public function storeClient(Request $request)
    {

        $client = Cliente::create([
            'nombre'        =>  $request->nombre,
            'tipo_pago'     =>  $request->tipo_pago,
            'telefono'      =>  $request->telefono,
            'direccion'     =>  $request->direccion
        ]);

        
        return 1;
    }

    public function clientGet()
    {
        $clientes = Cliente::all();

        return $clientes;
    }
}
