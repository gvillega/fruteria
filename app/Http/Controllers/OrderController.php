<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pedido;
use App\Models\Fruta;

class OrderController extends Controller
{
    public function index()
    {
        $pedido = Pedido::orderby('created_at','DESC')->take(1)->first();
        if(!isset($pedido)){
            $posicion = 1;
        }else{
            $posicion = $pedido->posicion;
        }

        return view('index', compact('posicion'));
    }

    public function get(Request $request)
    {   
        $pedido = Pedido::where('posicion', $request->posicion)->get();

        $response = ['list' => $pedido];
        
        return response()->json($response);
    }

    public function productoGet(){
        $fruta = Fruta::all();

        return $fruta;
    }

    public function store(Request $request)
    {
        $save = Pedido::create([
            'client_id'   =>  $request->client_id,
            'fruta_id'     =>  $request->fruta_id,
            'valor_kilo'   =>  $request->valor_kilo,
            'cantidad'     =>  $request->cantidad,
            'subtotal'     =>  $request->subtotal,
            'posicion'     =>  $request->posicion,
        ]);

        return 1;
    }

    public function delete(Request $request, $id)
    {
        $delete = Pedido::find($id)->delete();

        return 1;
    }
}
