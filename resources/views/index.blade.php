@extends('layouts.layout')

@section('content')

    <div class="container-fluid">
        <br><br>
        <div role="tabpanel">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Clientes</a>
                </li>
                <li role="presentation">
                    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Frutas</a>
                </li>
                <li role="presentation">
                    <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Pedido</a>
                </li>
            </ul><br><br>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h4>Datos del Cliente</h4></div>
                            <div class="panel-body">
                                <form id="client">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for:="nombre">Nombre</label>
                                            <input type="text" class="form-control" name="nombre">
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for:="tipo_pago">Tipo de Pago</label>
                                            <input type="text" class="form-control" name="tipo_pago">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for:="telefono">Telefono de contacto</label>
                                            <input type="text" class="form-control" name="telefono">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for:="numero_factura">N° Boleta/Factura</label>
                                            <input type="text" class="form-control" name="numero_factura">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for:="direccion">Dirección</label>
                                            <textarea class="form-control" name="direccion"></textarea>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-md-12">
                                <div class="col-md-offset-10">
                                    <input type="button" class="btn btn-danger" value="Borrar" id="trashClient">
                                    <input type="button" class="btn btn-primary" value="Guardar" id="saveClient">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h4>Pedido</h4></div>
                        <div class="panel-body">
                            <form id="orden">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for:="cliente">Cliente</label>
                                        <select class="form-control" name="client_id" id="client_id"></select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for:="producto">Producto</label>
                                        <select class="form-control" name="fruta_id" id="fruta_id"></select>
                                        <input type="hidden" class="form-control" name="posicion" value="{{$posicion}}" id="posicion">
                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for:="valor_kilo">Valor por kilo</label>
                                        <input type="number" class="form-control" name="valor_kilo" id="valor_kilo">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for:="cantidad">Cantidad</label>
                                        <input type="number" class="form-control" name="cantidad" id="cantidad">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for:="subtotal">SubTotal</label>
                                        <input type="text" class="form-control" name="subtotal" id="subtotal" readonly>
                                    </div>
                                </div><br>
                                <div class="col-md-12">
                                    <div class="col-md-offset-10">
                                        <input type="button" class="btn btn-danger" value="Borrar" id="btnBorrarPedido">
                                        <input type="button" class="btn btn-primary" value="Guardar" id="savePedido">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading"><h4>Pedido</h4></div>
                        <div class="panel-body">
                            <table id="pedido" class="display table table-striped table-bordered" style="width:100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    </div>
@endsection