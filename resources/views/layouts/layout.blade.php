<html>
    <head>
        <title>
            Fruteria
        </title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/datatable.css')}}">
    </head>
    <body>
        @yield('content')

        <script src="{{asset('js/jquery.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/jquery_datatable.js')}}"></script>
        <script src="{{asset('js/datatable.js')}}"></script>
        <script src="{{asset('js/mi_js.js')}}"></script>
        <script src="{{asset('js/client.js')}}"></script>
        <script>
            var $urlBase = "{{URL::to('/')}}";
        </script>
    </body>
</html>